# GIT Repository Mirroring Backup

## Description
Backup public GIT repositories

## Installation
1. Create a directory to store mirrored repositories
2. Clone desired repositories using 'git clone --mirror <REPOSITORY_URL>'
3. Run update command from parent directory in cron in desired intervals
