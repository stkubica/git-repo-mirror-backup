#!/bin/sh

# 1. Clone desired repositories using 'git clone --mirror <URL>'
# 2. Run update command from parent directory in cron


log() {
	echo "$(date +'%Y-%m-%d %H:%M:%S'): $1"
}

for dir in ./*; do
	if [ -d "$dir" ]; then
		log "Updating $dir repository"
		cd $dir
		git fetch -p origin
		cd ..
		sleep 4
	fi
done
